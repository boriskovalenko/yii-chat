<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\MessageForm;
use app\models\chat;
use app\models\Users;

class ChatController extends Controller
{
    public function actionChat() {
        $message = new MessageForm();
        if ($message->load(Yii::$app->request->post()) && $message->validate()) {
            $sender = new Users();
            $sender->id = $message->user;
            $sender->save();
            $dialog = new chat();
            $dialog->userId = $message->user;
            $dialog->text = $message->message;
            $dialog->time = date("Y-m-d H:i:s");
            $dialog->save();
            return $this->refresh();
        }
        $count = chat::find()->count();
        $chats = chat::find()->all();
        foreach ($chats as $chat) {
            $text[] = $chat->text;
            $UserId[] = $chat->userId;
        }

        return $this->render('chat', ['message' => $message, 'text' => $text, 'count' => $count, 'id' => $UserId]);
    }
}
