<?php

use yii\db\Schema;
use yii\db\Migration;

class m160207_175545_create_chat_table extends Migration
{
    public function up()
    {
        $this->createTable('chat', [
            'id' => $this->primaryKey(),
            'time' => $this->dateTime(),
            'userId' => $this->integer(),
            'text' => $this->text()
        ]);
        $this->addForeignKey('userId', 'chat', 'userId', 'users', 'id');
    }

    public function down()
    {
        $this->dropTable('chat');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
