<?php

namespace app\models;

use yii\base\Model;

class MessageForm extends Model
{
    public $message;
    public $user;

    public function rules()
    {
        return [
            [['message', 'user'], 'required']
        ];
    }
}