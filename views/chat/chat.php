<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<div class="container">
    <p>Окно сообщений:</p><br>
    <div>
        <?php
            session_set_cookie_params(1);
            $session = Yii::$app->session;
            if (!$session['userId']) {
                $session->set('userId', rand(0, 100));
            }
            $user = $session['userId'];
            for ($i=$count-6; $i<$count; $i++) {
                echo "@$id[$i]<br>";
                echo "&nbsp &nbsp". $text[$i]."<br>";
            }
        ?>
    </div>
</div>
<br>

<?php $form = ActiveForm::begin();?>
<?= $form->field($message, 'message') ?>
<?= $form->field($message, 'user')->hiddenInput(['value' => $user])->label($user) ?>


    <div class="form-group">
        <?= Html::submitButton('Отправить', ['class' => 'btn btn-primary']) ?>
    </div>

<?php ActiveForm::end(); ?>


